###Task: Build a simple chat application.  

Use AngularJS(AngularUI Bootsrap) for the front-end.  
Use Web API for the back-end web services.  
Use any DB storage mechanism you prefer.  

####Core features (MVP)
1. Users can POST chat messages to the WEB API endpoint.  
        a. These messages are recorded in the DB  
        b. You will have to consider what metadata needs to be recorded based on the MVP + Bonus features  
2. Users can GET chat messages from the endpoint  
        a. Default behaviour should be to load the last 20 chat messages  
        b. Do you think we always need to be pulling all of the chat messages since beginning of time?  
3. Users get a simple UI interface that:  
        a. Visualizes chat messages one under the other (most recent at the bottom)  
        b. Check with the server whether there are any new chat messages every 15 seconds (this need to be easily configurable via web-config!)  
        c. Input box for sending a new chat message  
        d. When a new chat message is sent, the chat should be refreshed immediately  

####Bonus features (do them in order)
1. When a user sends a smilie :) the chat room should visualize an actual smilie.  
        a. Make this extendable so that other smilie types can be added easily in the future  
2. Add a text box for users to set their name for the chat room.  
3. All messages in the chat room should now include who they were sent by.  
4. Messages that were sent by the current user should have "You" instead of their name.  
5. Add "connect" and "disconnect" buttons  
        a. Users now need to type their name before they hit "connect"  
        b. Users cannot see the chat room before they connect to it  
        c. You do not need to handle the scenario where a user closes their browser window instead of hitting disconnect  
6. Add a box which displays the list of people currently connected to the chat room  
        a. This box should refresh in the same way as the main chat window  
7. Add SignalR so that everything happens instantaneously instead of waiting for refresh (everything else should function identically)  
8. Add simple user management to allow a user to register and login to the chat room.  
        a. When a user first loads the page, they should have the option to login or register  
        b. Registration should require a username and password as well as a display name that they want to use for the chat room  
        c. Store the password in the database by hashing it with SHA512. Use a 64 byte salt to ensure password security.  
        d. Users should not be able to see the chat room until they login  
        e. When entering the chat room, the user's display name should default to what they selected during the registration process, but they should have the option to change it.  
        f. Users should be able to logout of the chat room (replace disconnect)  