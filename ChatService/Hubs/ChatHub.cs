﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

using ChatService.Controllers;
using System.Web.Http;
using ChatService.Models;
using System.Threading.Tasks;
using ChatService.Authentication;

namespace ChatService.Hubs
{
    public class ChatHub : Hub
    {
        private UserController userController = new UserController();
        private ChatController chatController = new ChatController();
        private LogInManager LogInManager = new LogInManager();

        public override Task OnConnected()
        {
            var token = Context.QueryString["token"];
            var connectionId = Context.ConnectionId;

            LogInManager.SetConnectionId(token, connectionId);

            return base.OnConnected();
        }
        public IHttpActionResult Send(Message message)
        {
            var response = chatController.PostMessage(message);
            
            return response;
        }
    }
}