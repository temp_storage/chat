﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatService.Models
{
    public class Smiley
    {
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public string Shortcode { get; set; }
        [Required]
        public string Emoticon { get; set; }
    }
}