﻿using ChatService.Authentication;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatService.Models
{
    public class Message
    {
        [NotMapped]
        private const long TicksPerSecond = 10000000;
        [NotMapped]
        private const long TicksFromBeginningToEpoch = 621355968000000000;
        public int Id { get; set; }
        //  simple solution to use datetime in the application
        //  and save the unix time to the database
        //  not used at the moment -> private
        //[NotMapped]
        //private DateTimeOffset DateTime
        //{
        //    get
        //    {
        //        return new DateTimeOffset(UnixTime * TicksPerSecond + TicksFromBeginningToEpoch,
        //            TimeSpan.Zero);
        //    }
        //    set
        //    {
        //        UnixTime = value.UtcTicks / TicksPerSecond - TicksFromBeginningToEpoch;
        //    }
        //}
        [Required]
        public long Timestamp { get; set; }
        [MaxLength(4000)]
        public string Body { get; set; }
        [MaxLength(100)]
        [Required]
        public string Author { get; set; }
        [Required]
        [ForeignKey("Account")]
        public int AccountId { get; set; }

        public virtual Account Account { get; set; }
    }
}