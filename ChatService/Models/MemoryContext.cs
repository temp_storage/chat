﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatService.Models
{
    public class MemoryContext
    {
        /// <summary>
        /// http://stackoverflow.com/questions/10736209/how-to-wrap-concurrentdictionary-in-blockingcollection
        /// research this later
        /// TODO: make it a lazy singleton
        /// </summary>
        private static readonly ConcurrentDictionary<string, User> _connectedUsers = new ConcurrentDictionary<string, User>();
        public static ConcurrentDictionary<string, User> ConnectedUsers
        {
            get
            {
                return _connectedUsers;
            }
        }
    }
}