﻿using ChatService.Authentication;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ChatService.Models
{
    public class ChatServiceContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }
        public DbSet<Smiley> Smilies { get; set; }
        public DbSet<Account> Accounts { get; set; }
    }
}