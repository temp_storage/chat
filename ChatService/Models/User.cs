﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatService.Models
{
    public class User
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Token { get; set; }
        [Required]
        public int AccountId { get; set; }
        public string ConnectionId { get; set; }
    }
}