﻿using ChatService.Hubs;
using ChatService.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatService.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private ConcurrentDictionary<string, User> ConnectedUsers = MemoryContext.ConnectedUsers;

        [Route("Connect")]
        [HttpPost]
        public IHttpActionResult Connect([FromBody]string name)
        {
            if (name == null)
                return BadRequest("Please provide a name");

            if (ConnectedUsers.Any(u => u.Name == name))
                return BadRequest("Please choose a different name");

            var user = new User { Name = name };

            ConnectedUsers.Add(user);
            GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients.All.loadConnectedUsers();

            return Ok();
        }

        [Route("Disconnect")]
        [HttpPost]
        public IHttpActionResult Disconnect([FromBody]string name)
        {
            if (name == null)
                return BadRequest("Please provide a name");

            if (ConnectedUsers.Any(u => u.Name == name))
            {
                var user = ConnectedUsers.Single(u => u.Name == name);

                ConnectedUsers.Remove(user);
                GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients.All.loadConnectedUsers();

                return Ok();
            }
            else
            {
                return BadRequest("User is not connected");
            }
        }

        [Route("ConnectedUsers")]
        [HttpGet]
        public IHttpActionResult GetUsers()
        {
            var token = ControllerContext.Request.Headers.Authorization.ToString();
            var connectionId = ConnectedUsers.Single(u => u.Token == token).ConnectionId;

            GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients.Client(connectionId).console();

            return Ok(ConnectedUsers);
        }
    }
}
