﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ChatService.Authentication;
using ChatService.Models;

namespace ChatService.Controllers
{
    [RoutePrefix("api/Authentication")]
    public class AccountController : ApiController
    {
        private AccountManager AccountManager = new AccountManager();
        private LogInManager LogInManager = new LogInManager();

        public AccountController()
        {

        }

        [HttpPost]
        [Route("LogIn")]
        public IHttpActionResult LogIn(AccountViewModel loginInfo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = LogInManager.LogIn(loginInfo.Username, loginInfo.Password);

            if (user == null)
            {
                return Ok(
                   new
                   {
                       success = false,
                       error = "Wrong credentials"
                   });
            }
            else
            {
                return Ok(
                    new
                    {
                        success = true,
                        Token = user.Token,
                        AccountId = user.AccountId
                    });
            }

        }
        [HttpPost]
        [Route("LogOut")]
        public IHttpActionResult LogOut()
        {
            var token = ControllerContext.Request.Headers.Authorization.ToString();

            if (token == null)
                return BadRequest("No Authorization header");

            var result = LogInManager.LogOut(token);

            if (result)
                return Ok(
                    new
                    {
                        success = true
                    });
            else
                return Ok(
                    new
                    {
                        success = false
                    });
        }

        [HttpPost]
        [Route("Register")]
        public IHttpActionResult Register(RegisterViewModel registrationInfo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var account = AccountManager.CreateAccount(registrationInfo.Username, registrationInfo.Password);

            if (account == null)
            {
                return Ok(
                   new
                   {
                       success = false,
                       error = "Unable to create an account"
                   });
            }
            else
            {
                return Ok(
                    new
                    {
                        success = true,
                        username = account.Username
                    });
            }
        }

        [HttpPost]
        [Route("ChangePassword")]
        public IHttpActionResult ChangePassword(ChangePasswordViewModel changePasswordInfo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var account = AccountManager.GetAccountInfo(changePasswordInfo.Username);
            if (account == null)
                return Ok(
                    new
                    {
                        success = false,
                        error = "User not found"
                    });
            var result = AccountManager.ChangePassword(account.Id, changePasswordInfo.OldPassword, changePasswordInfo.NewPassword);

            // split in if/else and provide error message
            return Ok(
                new
                {
                    success = result
                });
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}