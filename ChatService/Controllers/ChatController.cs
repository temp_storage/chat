﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using ChatService.Models;
using Microsoft.AspNet.SignalR;
using ChatService.Hubs;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace ChatService.Controllers
{
    [RoutePrefix("api/Chat")]
    public class ChatController : ApiController
    {
        private ChatServiceContext db = new ChatServiceContext();
        private ConcurrentDictionary<string, User> ConnectedUsers = MemoryContext.ConnectedUsers;

        // GET: api/Chat
        //[Route("chat/{numberOfMessages:int?}")]
        [Route("{numberOfMessages:int?}")]
        [HttpGet]
        public IHttpActionResult GetMessages(int numberOfMessages = 20)
        {
            return Ok(db.Messages
                .Select(m=> new
                {
                    m.AccountId,
                    m.Author,
                    m.Id,
                    m.Timestamp,
                    m.Body
                })
                .OrderByDescending(m => m.Timestamp)
                .Take(numberOfMessages)
                .OrderBy(m => m.Timestamp));
        }

        // POST: api/Chat
        [HttpPost]
        [Route("")]
        public IHttpActionResult PostMessage(Message message)
        {
            if (message == null)
            {
                return BadRequest("The message is null.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Messages.Add(message);
            db.SaveChanges();
            GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients.All.broadcastMessage(message);

            return Ok();
        }

        [Route("RefreshTime")]
        public IHttpActionResult GetRefreshTime()
        {
            return Ok(System.Configuration.ConfigurationManager.AppSettings["refreshTime"]);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}