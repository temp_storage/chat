﻿using ChatService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatService.Controllers
{
    [RoutePrefix("api/Smilies")]
    public class SmiliesController : ApiController
    {
        private ChatServiceContext db = new ChatServiceContext();

        // GET: api/Chat
        [HttpGet]
        public IHttpActionResult Smilies()
        {
            return Ok(db.Smilies);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
