﻿using ChatService.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatService.Authentication
{
    public class LogInManager
    {
        private AccountManager AccountManager = new AccountManager();
        private ConcurrentDictionary<string, User> ConnectedUsers = MemoryContext.ConnectedUsers;
        private TokenProvider TokenProvider = new TokenProvider();

        public User LogIn(string username, string password)
        {

            var account = AccountManager.GetAccountInfo(username);

            if (account == null)
                return null;

            var isPasswordCorrect = AccountManager.CheckPassword(account.Id, password);

            if (isPasswordCorrect)
            {

                var token = TokenProvider.GetToken();
                var user = new User
                {
                    Name = account.Username,
                    AccountId = account.Id,
                    Token = token,
                };

                ConnectedUsers.Add(user);


                return user;
            }
            else
            {
                return null;
            }
        }
        public bool SetConnectionId(string token, string connectionId)
        {
            var user = ConnectedUsers.SingleOrDefault(u => u.Token == token);

            if (user == null)
                return false;

            user.ConnectionId = connectionId;

            return true;
        }
        public bool LogOut(string token)
        {
            if (token == null)
                return false;

            var user = ConnectedUsers.SingleOrDefault(u => u.Token == token);

            if (user == null)
                return false;
            else
            {
                ConnectedUsers.Remove(user);
                return true;
            }
        }
    }
}