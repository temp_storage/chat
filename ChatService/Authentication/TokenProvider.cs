﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatService.Authentication
{
    public class TokenProvider
    {
        public string GetToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}