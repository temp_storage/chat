﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace ChatService.Authentication
{
    public class CryptoProvider
    {
        //  pass salt as a base64 encoded value and password in plaintext
        //  returns base64 encoded hash
        public string HashPassword(string salt, string password)
        {
            var sha512 = SHA512.Create();

            byte[] byteSalt = Convert.FromBase64String(salt);
            byte[] bytePassword = System.Text.Encoding.UTF8.GetBytes(password);

            byte[] combinedValue = byteSalt.Concat(bytePassword).ToArray();

            byte[] hash = sha512.ComputeHash(combinedValue);

            return Convert.ToBase64String(hash);
        }

        //  returns base64 encoded 64 byte random salt
        public string Generate64Salt()
        {
            var csrng = new RNGCryptoServiceProvider();
            var salt = new byte[64];

            csrng.GetBytes(salt);

            return Convert.ToBase64String(salt);
        }
    }
}