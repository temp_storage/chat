﻿using ChatService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatService.Authentication
{
    public class AccountManager
    {
        private ChatServiceContext db = new ChatServiceContext();
        private CryptoProvider CryptoProvider = new CryptoProvider();

        //  creates an account and returns the id
        public Account CreateAccount(string username, string password)
        {
            if (username == null || password == null)
                throw new Exception();

            if (db.Accounts.Any(a => a.Username == username))
                return null;

            var salt = CryptoProvider.Generate64Salt();
            var hash = CryptoProvider.HashPassword(salt, password);

            var account = db.Accounts.Add(new Account
            {
                Salt = salt,
                Hash = hash,
                Username = username
            });
            db.SaveChanges();

            return account;
        }

        public bool ChangePassword(int userId, string oldPassword, string newPassword)
        {
            if (CheckPassword(userId, oldPassword))
            {
                var result = UpdatePassword(userId, newPassword);

                return result;
            }
            else
                return false;
        }

        private bool UpdatePassword(int userId, string password)
        {
            var account = GetAccountInfo(userId);
            var newSalt = CryptoProvider.Generate64Salt();
            var newHash = CryptoProvider.HashPassword(newSalt, password);

            account.Salt = newSalt;
            account.Hash = newHash;
            db.SaveChanges();

            return true;
        }

        public bool CheckPassword(int userId, string password)
        {
            var account = GetAccountInfo(userId);

            var candidateHash = CryptoProvider.HashPassword(account.Salt, password);

            return account.Hash == candidateHash;
        }

        public Account GetAccountInfo(int userId)
        {
            var account = db.Accounts.SingleOrDefault(a => a.Id == userId);
            return account;
        }

        public Account GetAccountInfo(string username)
        {
            var account = db.Accounts.SingleOrDefault(a => a.Username == username);

            return account;
        }
    }
}