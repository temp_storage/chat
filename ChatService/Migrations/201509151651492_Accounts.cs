namespace ChatService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Accounts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        Hash = c.String(nullable: false),
                        Salt = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Messages", "AccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.Messages", "AccountId");
            AddForeignKey("dbo.Messages", "AccountId", "dbo.Accounts", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "AccountId", "dbo.Accounts");
            DropIndex("dbo.Messages", new[] { "AccountId" });
            DropColumn("dbo.Messages", "AccountId");
            DropTable("dbo.Accounts");
        }
    }
}
