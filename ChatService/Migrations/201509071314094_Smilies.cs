namespace ChatService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Smilies : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Smileys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(nullable: false),
                        Shortcode = c.String(nullable: false),
                        Emoticon = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Smileys");
        }
    }
}
