namespace ChatService.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ChatService.Models.ChatServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ChatService.Models.ChatServiceContext";
        }

        protected override void Seed(ChatService.Models.ChatServiceContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            //context.Smilies.Add(new Models.Smiley
            //{
            //    Url = "Content/Smilies/1F60D.png",
            //    Shortcode = ":heart:",
            //    Emoticon = "<3"
            //});
            //context.Smilies.Add(new Models.Smiley
            //{
            //    Url = "Content/Smilies/1F615.png",
            //    Shortcode = ":confused:",
            //    Emoticon = ":/"
            //});
            //context.Smilies.Add(new Models.Smiley
            //{
            //    Url = "Content/Smilies/1F636.png",
            //    Shortcode = ":speechless:",
            //    Emoticon = ":x"
            //});
            //context.Smilies.Add(new Models.Smiley
            //{
            //    Url = "Content/Smilies/1F641.png",
            //    Shortcode = ":frown:",
            //    Emoticon = ":("
            //});
            //context.Smilies.Add(new Models.Smiley
            //{
            //    Url = "Content/Smilies/1F642.png",
            //    Shortcode = ":smile:",
            //    Emoticon = ":)"
            //});
        }
    }
}
