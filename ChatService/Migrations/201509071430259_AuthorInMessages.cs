namespace ChatService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuthorInMessages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "Author", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "Author");
        }
    }
}
