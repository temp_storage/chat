#Notes on everything[1]

###Start with id(auto incr), timestamp, body

* id - auto incremented int;  
    Guid as an id sounds bad - not sequential. While timestamp cannot be a primary key,
    the order of incoming messages should be closely related to their timestamp.
    
* timestamp - int, epoch or datetime;  
    Epoch is the same everywhere - no need of conversions;
    Datetime is simpler as it's preformated and self explanatory but might also need conversions. 
    
* body - string, xml or plaintext;  
     Plaintext for now and extend to xml if there are any styling elements. 



###Database

* NoSQL - only if the SQL version doesn't deliver; might have issues on its own

* SQL with EF - simple, as EF acts as a repository.   

    If there are performance problems due to too large tables(messages), possible solutions are:  
    - partititioning the table(weekly/monthly/yearly or a week ago/a month ago/..);  
    - moving to NoSQL

    If ordering by timestamp becomes a bottleneck  -> composite key on (timestamp, id)

[1] - might split in different files/pages in the future



###Other

* Function updateMessages deals with updating the messages object in the vm;  
    Change to add only unique messages or make a new api method to request all since the last message or...  


###Todo
* Security
* Validation checks
* Update API index in ChatService
* IoC: add interfaces and dependency injection constructors everywhere
* return Created status code on post methods
* error messages in client
* consistency in " or '
*********
* check if everything is okay with the headers and connected users list
* make the list a concurrent dictionary
* change db links in smilies