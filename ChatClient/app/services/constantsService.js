﻿"use strict";
(function () {
    var app = angular.module("chat");

    var constantsService = function () {
        var serverURL = "http://localhost:44444"
        var signalRURL = serverURL + "/signalr";
        var apiURL = serverURL + "/api";
        var smiliesURL = apiURL + "/smilies";
        var messagesURL = apiURL + "/chat/";
        var refreshTimeURL = apiURL + "/chat/refreshTime";
        var connectURL = apiURL + "/user/connect";
        var disconnectURL = apiURL + "/user/disconnect";
        var connectedUsersURL = apiURL + "/user/connectedUsers";
        var loginURL = apiURL + "/authentication/login";
        var logoutURL = apiURL + "/authentication/logout";
        var registerURL = apiURL + "/authentication/register";


        return {
            smiliesURL: smiliesURL,
            loadMessagesURL: messagesURL,
            sendMessagesURL: messagesURL,
            refreshTimeURL: refreshTimeURL,
            connectURL: connectURL,
            disconnectURL: disconnectURL,
            connectedUsersURL: connectedUsersURL,
            signalRURL: signalRURL,
            loginURL: loginURL,
            logoutURL: logoutURL,
            registerURL: registerURL
        };
    };

    app.factory("constants", constantsService);
})();