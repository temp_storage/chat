﻿"use strict";
(function () {
    var app = angular.module("chat");

    var chatService = function ($http, constants) {
        var loadSmilies = function () {
            return $http.get(constants.smiliesURL);
        };

        var loadMessages = function () {
            return $http.get(constants.loadMessagesURL);
        };

        var loadRefreshTime = function () {
            return $http.get(constants.refreshTimeURL);
        };

        var sendMessage = function (message) {
            return $http.post(constants.sendMessagesURL, JSON.stringify(message));
        };

        return {
            loadSmilies: loadSmilies,
            loadMessages: loadMessages,
            loadRefreshTime: loadRefreshTime,
            sendMessage: sendMessage
        };
    };

    app.factory("chatService", ["$http", "constants", chatService]);
})();