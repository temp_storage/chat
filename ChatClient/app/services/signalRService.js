﻿"use strict";
(function () {
    var app = angular.module("chat");

    var signalRService = function (constants) {

        var chatHub = $.connection.chatHub;
        var client = chatHub.client;
        var server = chatHub.server;

        function connect() {
            $.connection.hub.url = constants.signalRURL, { useDefaultPath: false };
            return $.connection.hub.start()
        };

        function disconnect() {
            return $.connection.hub.stop()
        };

        function sendMessage(message) {
            return chatHub.server.send(message);
        };

        function setToken(token) {
            $.connection.hub.qs = { "token": token };
        };

        return {
            chatHub: chatHub,
            connect: connect,
            disconnect: disconnect,
            client: client,
            server: server,
            sendMessage: sendMessage,
            setToken: setToken
        };
    };
    app.factory("signalRService", ["constants", signalRService]);
})();