﻿"use strict";
(function () {
    var app = angular.module("chat");

    var userService = function ($http, $q, constants) {
        function connect(username) {
            return $http.post(constants.connectURL, JSON.stringify(username));
        };

        function disconnect(accountInformation) {
            return $http.post(constants.disconnectURL, JSON.stringify(accountInformation));
        };

        function connectedUsers() {
            return $http.get(constants.connectedUsersURL);
        };

        function login(loginInformation) {
            return $http.post(constants.loginURL, JSON.stringify(loginInformation));
        };

        function logout() {
            return $http.post(constants.logoutURL);
        };

        function register(registrationInformation) {
            return $http.post(constants.registerURL, JSON.stringify(registrationInformation));
        };

        function setToken(token)
        {
            var deferred = $q.defer();

            $http.defaults.headers.common.Authorization = token;

            deferred.resolve();

            return deferred.promise;
        }

        return {
            connect: connect,
            disconnect: disconnect,
            connectedUsers: connectedUsers,
            login: login,
            logout: logout,
            register: register,
            setToken: setToken
        };
    };

    app.factory("userService", ["$http", "$q", "constants", userService]);
})();