﻿"use strict";
(function () {
    var app = angular.module("chat");

    var smiliesService = function (chatService) {

        // escapes characters that are in the regex syntax to that they can be matched
        RegExp.escape = function (text) {
            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        };

        var loadSmilies = function () {
            chatService.loadSmilies()
            .then(function (response) {
                smilies = response.data;
            });
        };

        var parseSmilies = function (message) {
            var shortcodedMessage = message;

            for (var i = 0; i < smilies.length; i++) {

                var regex = new RegExp("(\\B|^)" + RegExp.escape(smilies[i].Emoticon) + "(?=(\\s|$))", "g");
                var replacement = smilies[i].Shortcode;

                shortcodedMessage = shortcodedMessage.replace(regex, replacement);
            }

            return shortcodedMessage;
        };

        var parseShortcodes = function (message) {
            var smilingMessage = message;

            for (var i = 0; i < smilies.length; i++) {
                var regex = new RegExp(RegExp.escape(smilies[i].Shortcode), "g");
                var replacement = "<img class='emoticon' src='" + smilies[i].Url + "'></img>";

                smilingMessage = smilingMessage.replace(regex, replacement);
            }

            return smilingMessage;
        };

        var parseMessage = function (message) {
            return parseShortcodes(parseSmilies(message));
        };

        var smilies = [];
        loadSmilies();

        return {
            parseSmilies: parseSmilies,
            parseShortcodes: parseShortcodes,
            parseMessage: parseMessage
        };
    };

    app.factory("smiliesService",["chatService" ,smiliesService])
})();