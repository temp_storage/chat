﻿"use strict";
(function () {
    var app = angular.module("chat");

    var connectController = function ($rootScope, $scope, $location, userService) {

        $scope.login = login;
        $scope.register = register;



        function register(username, password, confirmPassword) {
            var registrationInformation = {
                Username: username,
                Password: password,
                ConfirmPassword: confirmPassword
            };

            userService.register(registrationInformation)
            .then(function (response) {
                $scope.registerPassword = "";
                $scope.registerUsername = "";
                $scope.confirmPassword = "";
            });
        };

        function login(username, password) {
            var loginInformation = {
                Username: username,
                Password: password
            };

            userService.login(loginInformation)
            .then(function (response) {
                localStorage.setItem('username', username);
                localStorage.setItem('token', response.data.Token);
                localStorage.setItem('accountId', response.data.AccountId);
                userService.setToken(response.data.Token)
                .then(function () {
                    $location.path("/chat");

                });
            });
        };
    };

    app.controller("ConnectController", ["$rootScope", "$scope", "$location", "userService", connectController]);
})();