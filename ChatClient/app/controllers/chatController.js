﻿"use strict";
/// <reference path="angular.js" />
(function () {
    var module = angular.module("chat");

    var ChatController = function ($rootScope, $scope, $q, $location, chatService, signalRService, userService, smiliesService) {

        $scope.sendMessage = sendMessage;
        $scope.disconnect = disconnect;
        $scope.logout = logout;

        //  redirect if no one's logged in
        if (localStorage.getItem("token") === undefined || localStorage.getItem("token") === null)
            $location.path("/login");
        else {
            $scope.username = localStorage.getItem("username");
            $scope.accountId = Number(localStorage.getItem("accountId"));
            init();
        }

        //$scope.$on('$viewContentLoaded', function () {
        //    init();
        //});

        function init() {

            // methods must be attached to hub.client before connecting to the hub (start())
            // http://www.asp.net/signalr/overview/guide-to-the-api/hubs-api-guide-javascript-client#nogenconnection
            // attach client-side methods
            configureSignalR()
            .then(function () {
                startAndLoad();
            });
        };

        function configureSignalR() {
            var deferred = $q.defer();
            signalRService.client.console = function (msg) { console.log(msg) };
            signalRService.client.broadcastMessage = addMessageToScope;
            signalRService.client.loadConnectedUsers = loadConnectedUsers;
            signalRService.setToken(localStorage.getItem("token"));

            deferred.resolve();

            return deferred.promise;

        };

        // connect and display messages and other connected users on success
        function startAndLoad() {
            signalRService.connect()
                .done(function () {
                    loadMessages();
                    loadConnectedUsers();
                })
                .fail(function () {
                    console.log("Couldn't connect");
                });
        }

        // this function handles how retrieved messages are added to the view
        // currently it just replaces with the new ones
        function updateMessages(retrievedMessages) {
            for (var i = 0; i < retrievedMessages.length; i++) {
                retrievedMessages[i].Body = smiliesService.parseMessage(retrievedMessages[i].Body);
            }

            $scope.messages = retrievedMessages;

        };

        function loadMessages() {
            chatService.loadMessages()
                .then(function (response) {

                    var retrievedMessages = response.data;

                    if (retrievedMessages.length === 0)
                        onError("no messages");

                    updateMessages(retrievedMessages);
                });
        };

        function scrollToBottom() {
            var chatWindow = angular.element(".messages-window ul");
            chatWindow.scrollTop(chatWindow.height());
        };

        function sendMessage() {
            var message = {
                Author: $scope.username,
                // todo: extract it from the token 
                AccountId: $scope.accountId,
                Timestamp: Date.now(),
                Body: $scope.messageText
            };

            signalRService.sendMessage(message);

            $scope.messageText = "";
            angular.element("#textBox").focus();

            $scope.inputForm.$setPristine();
            $scope.inputForm.$setUntouched();

        };

        function loadConnectedUsers() {
            userService.connectedUsers()
            .then(function (response) {
                $scope.connectedUsers = response.data;
            });
        };

        function disconnect() {
            userService.disconnect($rootScope.username)
            .then(function () {
                $location.path("/login");
            });
        };
        function logout() {
            userService.logout()
            .then(function () {
                $scope.username = "";
                localStorage.removeItem("username");
                localStorage.removeItem("token");
                localStorage.removeItem("accountId");
                signalRService.disconnect();
                $location.path("/login");
            });
        };
        function addMessageToScope(message) {
            $scope.messages.push(message);
            $scope.$apply();
            scrollToBottom();
        };




    };

    module.controller("ChatController", ["$rootScope", "$scope", "$q", "$location", "chatService", "signalRService", "userService", "smiliesService", ChatController]);
})();