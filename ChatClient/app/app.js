﻿"use strict";
(function () {
    var app = angular.module("chat", ["ngRoute", "ngSanitize"]);
    
    app.config(function ($routeProvider) {
        $routeProvider
        .when("/login", {
            templateUrl: "app/views/connect.html",
            controller: "ConnectController"
        })
        .when("/chat", {
            templateUrl: "app/views/chat.html",
            controller: "ChatController"
        })
        .otherwise({
            redirectTo: "/login"
        });
    });
})();
